package cl.wom.ws.tools;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 
 * @author Lucas Sánchez
 * 
 * Class to test UtilitiesTest Class.
 *
 */
@SpringBootTest
public class UtilitiesTest {
	
	/**
	 * Method to test convertToDateyyyyMMddHHmmssSSSZ method.
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testConvertStringToDate() throws ParseException {
		Date date = Utilities.convertToDateyyyyMMddHHmmssSSSZ("2019-08-02T23:16:53.646Z");
		
		assertThat(date.getHours()).isEqualTo(23);
		
		assertThat(date).isInstanceOf(Date.class);
		
		assertThat(date.toString()).isEqualTo("Fri Aug 02 23:16:53 COT 2019");
	}
	
	/**
	 * Method  to test convertyyyMMddHHmmssSSSZToSqlDate method.
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testConvertStringToSqlDate() throws ParseException {
		java.sql.Date date = Utilities.convertyyyMMddHHmmssSSSZToSqlDate("2019-08-02T23:16:53.646Z");
		
		assertThat(date).isInstanceOf(java.sql.Date.class);
		
		assertThat(date.toString()).isEqualTo("2019-08-02");
	}
}
