package cl.wom.ws.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Lucas Sánchez
 * 
 * This class contains helper methods.
 *
 */
public class Utilities {
	/**
	 * This method converts strings with 2019-08-02T23:16:53.646Z format to date
	 * 
	 * @param pString
	 * @return
	 * @throws ParseException
	 */
	public static Date convertToDateyyyyMMddHHmmssSSSZ(String pString) throws ParseException {
		Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(pString);
		return date;
	}
	
	/**
	 * This method convert date to sqlDate
	 * 
	 * @param pString
	 * @return
	 * @throws ParseException
	 */
	public static java.sql.Date convertyyyMMddHHmmssSSSZToSqlDate(String pString) throws ParseException{
		Date utilDate = convertToDateyyyyMMddHHmmssSSSZ(pString);
		java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
		return sqlDate;
	}

}
