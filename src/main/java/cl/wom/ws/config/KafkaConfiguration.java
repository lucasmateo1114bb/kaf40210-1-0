package cl.wom.ws.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.SeekToCurrentErrorHandler;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import cl.wom.ws.constants.KafkaServerConfigConstants;
import cl.wom.ws.model.InventoryAdjustment;

/**
 * @author Lucas Sánchez
 * Kafka Configuration
 */
@EnableKafka
@Configuration
public class KafkaConfiguration {
	
	/**
	 * Consumer factory to get json message as string
	 * @return
	 */
	@Bean
	public ConsumerFactory<String, String> consumerFactory(){
		Map<String, Object> configs = new HashMap<>();
		configs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaServerConfigConstants.SERVER_IP+":"+KafkaServerConfigConstants.SERVER_PORT);
		configs.put(ConsumerConfig.GROUP_ID_CONFIG, KafkaServerConfigConstants.GROUP_ID);
		configs.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		configs.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		
		return new DefaultKafkaConsumerFactory<>(configs);
	}
	
	/**
	 * Consumer container where consumerFactory is used
	 * @return
	 */
	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory());
		return factory;
	}	
	
	/**
	 * Consumer factory to receive message as InventoryAdjustment object
	 * 
	 * @return
	 */
	@Bean
	public ConsumerFactory<String, InventoryAdjustment> inventoryAdjustmentConsumerFactory(){
		Map<String, Object> configs = new HashMap<>();
		configs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaServerConfigConstants.SERVER_IP+":"+KafkaServerConfigConstants.SERVER_PORT);
		configs.put(ConsumerConfig.GROUP_ID_CONFIG, KafkaServerConfigConstants.GROUP_ID);
		configs.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		configs.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
		configs.put(ErrorHandlingDeserializer2.KEY_DESERIALIZER_CLASS, StringDeserializer.class);
		configs.put(ErrorHandlingDeserializer2.VALUE_DESERIALIZER_CLASS, JsonDeserializer.class);
		
		return new DefaultKafkaConsumerFactory<>(configs, 
				new StringDeserializer(), 
				new JsonDeserializer<>(InventoryAdjustment.class));
	}
	
	/**
	 * Consumer Container where inventoryAdjustmentConsumerFactory is used
	 * 
	 * @return
	 */
	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, InventoryAdjustment> inventoryAdjustmentKafkaListenerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, InventoryAdjustment> factory = new ConcurrentKafkaListenerContainerFactory<>();
		// This is for handle bad messages
		factory.setErrorHandler(new SeekToCurrentErrorHandler());
		factory.setConsumerFactory(inventoryAdjustmentConsumerFactory());
		return factory;
	}
}
