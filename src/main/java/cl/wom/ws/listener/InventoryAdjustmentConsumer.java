package cl.wom.ws.listener;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import cl.wom.ws.model.InventoryAdjustment;
import cl.wom.ws.model.InventoryAdjustmentData;
import cl.wom.ws.tools.Utilities;
import cl.wom.ws.constants.KafkaServerConfigConstants;
import cl.wom.ws.database.repository.IInventoryAdjustmentRepository;

/**
 * 
 * @author Lucas Sánchez
 * 
 * Inventory Adjustment Consumer Service
 * 
 */
@Service
public class InventoryAdjustmentConsumer {
	
	/**
	 * IInventoryAdjustmentRepository propertie
	 */
	@Autowired
	private IInventoryAdjustmentRepository inventoryAdjustmentRepo;
	
	/**
	 * Kafka Template
	 */
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	@KafkaListener(topics=KafkaServerConfigConstants.OPERATION_ERROR_TOPIC, groupId=KafkaServerConfigConstants.GROUP_ID)
	public void operationError(String  pMessage) {
		//Its not specified what I shuld do when this happen
		System.out.println(pMessage);
	}
	
	@KafkaListener(topics=KafkaServerConfigConstants.OPERATION_SUCCESS_TOPIC, groupId=KafkaServerConfigConstants.GROUP_ID)
	public void operationSuccess(String  pMessage) {
		//Its not specified what I shuld do when this happen
		System.out.println(pMessage);
	}
	
	/**
	 * inventoryAdjustmentListener listener for inventoryAdjustmentKafkaListenerFactory topic.
	 * @param pInventoryAdjustment
	 */
	@KafkaListener(topics=KafkaServerConfigConstants.INVENTORY_ADJUSTMENT_TOPIC, 
			groupId=KafkaServerConfigConstants.GROUP_ID,
			containerFactory = KafkaServerConfigConstants.INVENTORY_ADJUSTMENT_CONTAINER_FACTORY)
	public void inventoryAdjustmentListener(InventoryAdjustment pInventoryAdjustment) {
		try {	
			System.out.println("Message:: "+pInventoryAdjustment);
			ArrayList<cl.wom.ws.database.model.InventoryAdjustment> inventoryAdjustmentsArray = new ArrayList<cl.wom.ws.database.model.InventoryAdjustment>();
			cl.wom.ws.database.model.InventoryAdjustment inventoryAdjustmentToSave;
			// pInventoryAdjustment.getData() is iterated in case more than one object.
			for(InventoryAdjustmentData data : pInventoryAdjustment.getData()) {
				//System.out.println(data.getCanalVenta());
				
				inventoryAdjustmentToSave = new cl.wom.ws.database.model.InventoryAdjustment();
				
				/*
				 * Despite message comes with id value, the excel document say use sequence
				 * Because that inventoryAdjustmentToSave.setId( is not used
				 */
				
				inventoryAdjustmentToSave.setTransactionDate(Utilities.convertyyyMMddHHmmssSSSZToSqlDate(data.getTransactionDate()));
				inventoryAdjustmentToSave.setCodigoSku(data.getCodigoSku());
				inventoryAdjustmentToSave.setCantidad(data.getCantidad());
				inventoryAdjustmentToSave.setSerie(data.getSerie());
				inventoryAdjustmentToSave.setCodOrganizacionDesde(data.getCodOrganizacionDesde());
				inventoryAdjustmentToSave.setSubinventoryCodeDesde(data.getSubinventoryCodeDesde());
				inventoryAdjustmentToSave.setCodOrganizacionHasta(data.getCodOrganizacionHasta());
				inventoryAdjustmentToSave.setSubinventoryCodeHasta(data.getSubinventoryCodeHasta());
				inventoryAdjustmentToSave.setTransactionType(data.getTransactionType());
				inventoryAdjustmentToSave.setCreatedBy(data.getCreatedBy());
				inventoryAdjustmentToSave.setCreatedDate(Utilities.convertyyyMMddHHmmssSSSZToSqlDate(data.getCreatedDate()));
				inventoryAdjustmentToSave.setLastUpdateBy(data.getLastUpdateBy());
				inventoryAdjustmentToSave.setLastUpdateDate(Utilities.convertyyyMMddHHmmssSSSZToSqlDate(data.getLastUpdateDate()));
				inventoryAdjustmentToSave.setUnidadMedida(data.getUnidadMedida());
				inventoryAdjustmentToSave.setCanalVenta(data.getCanalVenta());
				inventoryAdjustmentToSave.setUnidadOperativa(data.getUnidadOperativa());
				inventoryAdjustmentToSave.setAtributte1(data.getAtributte1());
				inventoryAdjustmentToSave.setAtributte2(data.getAtributte2());
				inventoryAdjustmentToSave.setAtributte3(data.getAtributte3());
				inventoryAdjustmentToSave.setAtributte4(data.getAtributte4());
				inventoryAdjustmentToSave.setAtributte5(data.getAtributte5());
				inventoryAdjustmentsArray.add(inventoryAdjustmentToSave);				
			}
			inventoryAdjustmentRepo.saveAll(inventoryAdjustmentsArray);
			//System.out.print(inventoryAdjustmentRepo.findAll());
			kafkaTemplate.send(KafkaServerConfigConstants.OPERATION_SUCCESS_TOPIC, "Process has ended correctly");
		} catch (Exception e) {
			kafkaTemplate.send(KafkaServerConfigConstants.OPERATION_ERROR_TOPIC, e.getMessage());
			e.printStackTrace();
		}		
	}
}
