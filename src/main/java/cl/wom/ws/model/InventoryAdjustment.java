package cl.wom.ws.model;

import java.util.ArrayList;
import lombok.Data;

/**
 * @author Lucas Sánchez
 * 
 * This class represents json Message.
 */
@Data
public class InventoryAdjustment {
	
	/**
	 * id from json message.
	 */
	private String id;
	
	/**
	 * user from json message.
	 */
	private String user;
	
	/**
	 * nombreOperacion from json message.
	 */
	private String nombreOperacion;
	
	/**
	 * data from json message.
	 */
	private ArrayList<InventoryAdjustmentData> data;
	
	/**
	 * fecha_mensaje from json message.
	 */
	private long fecha_mensaje;
}
