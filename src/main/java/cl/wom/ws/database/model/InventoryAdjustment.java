package cl.wom.ws.database.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

/**
 * 
 * @author Lucas Sánchez
 * 
 * This class represent InventoryAdjustment on DB
 *
 */
@Entity
@Table(name="XXNCL_INV_WMSAJUSTES_INTERFACE")
@Data
public class InventoryAdjustment {
	/**
	 * ID único por registro insertado. Se utiliza secuencia.
	 */
	@Id
	@Column(name="ID")
	@SequenceGenerator(name = "sequence", initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
	private int id;
	
	/**
	 * Unidad operativa
	 */
	@Column(name="UNIDAD_OPERATIVA")
	private String unidadOperativa;
	
	/**
	 * Organización Origen. Es el código corto usado para identificar las tiendas. Ejemplo FSN
	 */
	@Column(name="ORGORIGEN", nullable = false)
	private String codOrganizacionDesde;
	
	/**
	 * Organización Destino. Es el código corto usado para identificar las tiendas. Ejemplo FSN
	 */
	@Column(name="ORGDESTINO")
	private String codOrganizacionHasta;
	
	/**
	 * Es motivo  o tipo de transacción que necesita integrar. Por Ejemplo, Ventas, Ajustes
	 */
	@Column(name="MOTIVO")
	private String transactionType;
	
	/**
	 * Código  del Material. Ejemplo: 001.002.1540
	 */
	@Column(name="CODIGOMATERIAL", nullable = false)
	private String codigoSku;
	
	/**
	 * Unidad de medida primaria del SKU
	 */
	@Column(name="UNIDADMEDIDA")
	private String unidadMedida;
	
	/**
	 * Número de unidades a procesar. Puede ser 1 ó -1
	 */
	@Column(name="CANTIDAD", nullable = false)
	private double cantidad;
	
	/**
	 * Canal  por donde se realiza la venta
	 */
	@Column(name="CANAL_VENTA", nullable = false)
	private String canalVenta;
	
	/**
	 * Número de la serie que se está transando.
	 */
	@Column(name="NUMEROSERIE", nullable = false)
	private String serie;
	
	/**
	 * Código  de la bodega donde se realiza la transacción. Ejemplo: NUEVOS
	 */
	@Column(name="SUBINVORIGEN", nullable = false)
	private String subinventoryCodeDesde;
	
	/**
	 * Fecha de la transacción
	 */
	@Column(name="FECHATRANSACCION", nullable = false)
	private Date transactionDate;
	
	/**
	 * Código  de la bodega donde se realiza la transacción. Ejemplo: NUEVOS
	 */
	@Column(name="SUBINVDESTINO")
	private String subinventoryCodeHasta;
	
	/**
	 * Libre para valores de integración
	 */
	@Column(name="ATRIBUTTE1")
	private String atributte1;
	
	/**
	 * Libre para valores de integración
	 */
	@Column(name="ATRIBUTTE2")
	private String atributte2;
	
	/**
	 * Libre para valores de integración
	 */
	@Column(name="ATRIBUTTE3")
	private String atributte3;
	
	/**
	 * Libre para valores de integración
	 */
	@Column(name="ATRIBUTTE4")
	private String atributte4;
	
	/**
	 * Libre para valores de integración
	 */
	@Column(name="ATRIBUTTE5")
	private String atributte5;
	
	/**
	 * Usuario que creo el  registro
	 */
	@Column(name="CREATED_BY", nullable = false)
	private int createdBy;
	
	/**
	 * Fecha de creación del  registro
	 */
	@Column(name="CREATION_DATE", nullable = false)
	private Date createdDate;
	
	/**
	 * Fecha de la última actualización
	 */
	@Column(name="LAST_UPDATED_DATE", nullable = false)
	private Date lastUpdateDate;
	
	/**
	 * Usuario de la última actualización
	 */
	@Column(name="LAST_UPDATE_BY", nullable = false)
	private int lastUpdateBy;
}
