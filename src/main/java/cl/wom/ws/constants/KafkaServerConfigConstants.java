package cl.wom.ws.constants;


/**
 * @author Lucas Sánchez
 * This class contains constants about kafaka server configuration.
 *
 */
public class KafkaServerConfigConstants {
	/**
	 * Constant to hold Kafka Server IP
	 */
	public static final String SERVER_IP = "172.18.0.2";
	
	/**
	 * Constant to hold Kafka Server Port
	 */
	public static final String SERVER_PORT = "9092";
	
	/**
	 * Constant to hold Kafka Group Id 
	 */
	public static final String GROUP_ID = "group_id";
	
	/**
	 * Constant to hold InventoryAdjusment topic
	 */
	public static final String INVENTORY_ADJUSTMENT_TOPIC = "KAF40021-1-0-SupplyChainErpService-InventoryAdjusment";
	
	/**
	 * Constant to hold method name of InventoryAdjusment Listener Factory
	 */
	public static final String INVENTORY_ADJUSTMENT_CONTAINER_FACTORY = "inventoryAdjustmentKafkaListenerFactory";
	
	public static final String OPERATION_ERROR_TOPIC = "KAF40022-1-0-SupplyChainErpService-OperationError";
	
	public static final String OPERATION_SUCCESS_TOPIC = "KAF40023-1-0-SupplyChainErpService-OperationSuccess";
}
