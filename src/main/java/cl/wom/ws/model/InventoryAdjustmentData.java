package cl.wom.ws.model;

import lombok.Data;

/**
 * @author Lucas Sánchez
 * 
 * This class represents data field from json Message.
 */
@Data
public class InventoryAdjustmentData {
	
	/**
	 * Constructor.
	 */
	public InventoryAdjustmentData() {
		super();
	}

	/**
	 * unidadOperativa from json message.
	 */
	private String unidadOperativa;
	
	/**
	 * codOrganizacionDesde from json message.
	 */
	private String codOrganizacionDesde;
	
	/**
	 * codOrganizacionHasta from json message.
	 */
	private String codOrganizacionHasta;
	
	/**
	 * transactionType from json message.
	 */
	private String transactionType;
	
	/**
	 * codigoSku from json message.
	 */
	private String codigoSku;
	
	/**
	 * unidadMedida from json message.
	 */
	private String unidadMedida;
	
	/**
	 * cantidad from json message.
	 */
	private double cantidad;
	
	/**
	 * canalVenta from json message.
	 */
	private String canalVenta;
	
	/**
	 * serie from json message.
	 */
	private String serie;
	
	/**
	 * subinventoryCodeDesde from json message.
	 */
	private String subinventoryCodeDesde;
	
	/**
	 * transactionDate from json message.
	 */
	private String transactionDate;
	
	/**
	 * subinventoryCodeHasta from json message.
	 */
	private String subinventoryCodeHasta;
	
	/**
	 * atributte1 from json message.
	 */
	private String atributte1;
	
	/**
	 * atributte2 from json message.
	 */
	private String atributte2;
	
	/**
	 * atributte3 from json message.
	 */
	private String atributte3;
	
	/**
	 * atributte4 from json message.
	 */
	private String atributte4;
	
	/**
	 * atributte5 from json message.
	 */
	private String atributte5;
	
	/**
	 * createdBy from json message.
	 */
	private int createdBy;
	
	/**
	 * createdDate from json message.
	 */
	private String createdDate;
	
	/**
	 * lastUpdateDate from json message.
	 */
	private String lastUpdateDate;
	
	/**
	 * lastUpdateBy from json message.
	 */
	private int lastUpdateBy;
	
	
}
