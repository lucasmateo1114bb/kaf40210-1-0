package cl.wom.ws.listener;

import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.KafkaTemplate;

import cl.wom.ws.constants.KafkaServerConfigConstants;
import cl.wom.ws.model.InventoryAdjustment;
import cl.wom.ws.database.repository.IInventoryAdjustmentRepository;
import cl.wom.ws.model.InventoryAdjustmentData;
import cl.wom.ws.tools.Utilities;

@SpringBootTest
public class InventoryAdjustmentConsumerTest {
	
	@Autowired
	private InventoryAdjustmentConsumer inventoryAdjustmentConsumer;
	
	@MockBean
	private KafkaTemplate<String, String> kafkaTemplate;
	
	
	@MockBean
	private IInventoryAdjustmentRepository inventoryAdjustmentRepo;
	
	@Test
	public void inventoryAdjustmentListenerTest() throws ParseException{
		InventoryAdjustmentData invAdjustmentData = new InventoryAdjustmentData();
		InventoryAdjustment invAdjusment = new InventoryAdjustment();
		ArrayList<InventoryAdjustmentData> invAdjustmentDataArray = new ArrayList<InventoryAdjustmentData>();
		
		cl.wom.ws.database.model.InventoryAdjustment invAdjusmentDB = new cl.wom.ws.database.model.InventoryAdjustment();
		ArrayList<cl.wom.ws.database.model.InventoryAdjustment> invAdjustmenDBArray = new ArrayList<cl.wom.ws.database.model.InventoryAdjustment>();
		
		//Consumer message		
		invAdjustmentData.setUnidadOperativa("WOMMA");
		invAdjustmentData.setCodOrganizacionDesde("AAA");
		invAdjustmentData.setCodOrganizacionHasta("ZZZ");
		invAdjustmentData.setTransactionDate("2019-08-02T23:16:53.646Z");
		invAdjustmentData.setCreatedDate("2019-08-02T23:16:53.646Z");
		invAdjustmentData.setLastUpdateDate("2019-08-02T23:16:53.646Z");
		
		invAdjustmentDataArray.add(invAdjustmentData);
		
		invAdjusment.setId("43d84da9-a31f-47c7-89d4-20ed615a090f");
		invAdjusment.setUser("roymer");
		invAdjusment.setNombreOperacion("inventoryAdjustment");
		invAdjusment.setFecha_mensaje(1575643453380L);
		invAdjusment.setData(invAdjustmentDataArray);
		
		//Inventory Adjustment Entity
		invAdjusmentDB.setTransactionDate(Utilities.convertyyyMMddHHmmssSSSZToSqlDate(invAdjustmentData.getTransactionDate()));
		invAdjusmentDB.setCodigoSku(invAdjustmentData.getCodigoSku());
		invAdjusmentDB.setCantidad(invAdjustmentData.getCantidad());
		invAdjusmentDB.setSerie(invAdjustmentData.getSerie());
		invAdjusmentDB.setCodOrganizacionDesde(invAdjustmentData.getCodOrganizacionDesde());
		invAdjusmentDB.setSubinventoryCodeDesde(invAdjustmentData.getSubinventoryCodeDesde());
		invAdjusmentDB.setCodOrganizacionHasta(invAdjustmentData.getCodOrganizacionHasta());
		invAdjusmentDB.setSubinventoryCodeHasta(invAdjustmentData.getSubinventoryCodeHasta());
		invAdjusmentDB.setTransactionType(invAdjustmentData.getTransactionType());
		invAdjusmentDB.setCreatedBy(invAdjustmentData.getCreatedBy());
		invAdjusmentDB.setCreatedDate(Utilities.convertyyyMMddHHmmssSSSZToSqlDate(invAdjustmentData.getCreatedDate()));
		invAdjusmentDB.setLastUpdateBy(invAdjustmentData.getLastUpdateBy());
		invAdjusmentDB.setLastUpdateDate(Utilities.convertyyyMMddHHmmssSSSZToSqlDate(invAdjustmentData.getLastUpdateDate()));
		invAdjusmentDB.setUnidadMedida(invAdjustmentData.getUnidadMedida());
		invAdjusmentDB.setCanalVenta(invAdjustmentData.getCanalVenta());
		invAdjusmentDB.setUnidadOperativa(invAdjustmentData.getUnidadOperativa());
		invAdjusmentDB.setAtributte1(invAdjustmentData.getAtributte1());
		invAdjusmentDB.setAtributte2(invAdjustmentData.getAtributte2());
		invAdjusmentDB.setAtributte3(invAdjustmentData.getAtributte3());
		invAdjusmentDB.setAtributte4(invAdjustmentData.getAtributte4());
		invAdjusmentDB.setAtributte5(invAdjustmentData.getAtributte5());
		invAdjustmenDBArray.add(invAdjusmentDB);
		
		when(kafkaTemplate.send(KafkaServerConfigConstants.OPERATION_SUCCESS_TOPIC, "Process has ended correctly")).thenReturn(null);
		when(inventoryAdjustmentRepo.saveAll(invAdjustmenDBArray)).thenReturn(null);
		
		// This is a very long test... but you know what I meant
		
	}
}
