package cl.wom.ws.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.wom.ws.database.model.InventoryAdjustment;

/**
 * 
 * @author Lucas Sánchez
 * 
 * Interface to control data flow of InventoryAdjustment
 *
 */
public interface IInventoryAdjustmentRepository extends JpaRepository<InventoryAdjustment, Integer>{

}
